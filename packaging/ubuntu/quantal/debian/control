Source: xnoise
Section: sound
Priority: optional
Maintainer: Jörn Magens <shuerhaaken@googlemail.com>
Build-Depends: debhelper (>= 8), intltool, pkg-config, libsqlite3-dev (>= 3.6), libgtk-3-dev (>=3.4), libgstreamer0.10-dev (>=0.10.28), libgstreamer-plugins-base0.10-dev (>=0.10.28), libcairo2-dev, libxml2-dev (>= 2.7.0), libsoup2.4-dev, libx11-dev, libubuntuoneui-dev, libtag1-dev (>=1.6.0)
Standards-Version: 3.9.3
Homepage: http://www.xnoise-media-player.com/


Package: xnoise
Architecture: any
Depends: gstreamer0.10-plugins-base, gstreamer0.10-plugins-good, ${misc:Depends}, ${shlibs:Depends}
Recommends: tumbler, gnome-icon-theme-symbolic
Description: Pretty and fast media player for Gtk+
 Xnoise allows listening to music and playing video in an intuitive way: You 
 can easily search the library and drag each artist, album or title to the 
 tracklist (to any position in any order). In the tracklist all queued tracks 
 (music or video) are played one by one without being removed. There, you can 
 reorder, insert or remove any track as it comes to your mind.
 .
 The media library contains all available media as a hierarchical tree 
 structure of media tag metadata. It is easy to find any single track, artist, 
 album or genre by using this tree structure or by entering a search term.
 Here you find all you music, streams or videos in a nicely presented, 
 searchable way. 
 .
 Xnoise can play every kind of audio/video data that GStreamer can handle.
 There are plugins available.
 .
 This package contains the xnoise player itself, and the core plugins.


Package: xnoise-ubuntuone-music-store-plugin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, xnoise (= ${binary:Version})
Description: Pretty and fast media player for Gtk+ (UbuntuOneMusic plugin)
 Xnoise allows listening to music and playing video in an intuitive way: You 
 can easily search the library and drag each artist, album or title to the 
 tracklist (to any position in any order). In the tracklist all queued tracks 
 (music or video) are played one by one without being removed. There, you can 
 reorder, insert or remove any track as it comes to your mind.
 .
 The media library contains all available media as a hierarchical tree 
 structure of media tag metadata. It is easy to find any single track, artist, 
 album or genre by using this tree structure or by entering a search term.
 Here you find all you music, streams or videos in a nicely presented, 
 searchable way. 
 .
 Xnoise can play every kind of audio/video data that GStreamer can handle.
 There are plugins available.
 .
 This package contains the plugins for xnoise media player.


Package: xnoise-dev
Architecture: any
Depends: ${misc:Depends}, xnoise (= ${binary:Version})
Description: Pretty and fast media player for Gtk+ (development)
 Xnoise allows listening to music and playing video in an intuitive way: You 
 can easily search the library and drag each artist, album or title to the 
 tracklist (to any position in any order). In the tracklist all queued tracks 
 (music or video) are played one by one without being removed. There, you can 
 reorder, insert or remove any track as it comes to your mind.
 .
 The media library contains all available media as a hierarchical tree 
 structure of media tag metadata. It is easy to find any single track, artist, 
 album or genre by using this tree structure or by entering a search term.
 Here you find all you music, streams or videos in a nicely presented, 
 searchable way. 
 .
 Xnoise can play every kind of audio/video data that GStreamer can handle.
 There are plugins available.
 .
 This package contains the vapis and header files which are needed for 
 developing Xnoise plugins.

