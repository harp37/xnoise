msgid ""
msgstr ""
"Project-Id-Version: xnoise\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-02-11 13:52+0100\n"
"PO-Revision-Date: 2009-09-20 20:08+0530\n"
"Last-Translator: \n"
"Language-Team:  \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"\n"

#: ../data/misc/xnoise.desktop.in.in.h:1
msgid "Xnoise"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:2
msgid "Media Player"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:3
msgid "Organize and play your music and video collections"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:4
#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:73
msgid "Play/Pause"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:5
msgid "Stop"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:6
msgid "Goto next track"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:7
msgid "Goto previous track"
msgstr ""

#: ../data/misc/xnoise.desktop.in.in.h:8
msgid "Quit application"
msgstr ""

#: ../libxnoise/Application/xnoise-application.vala:244 ../src/xnoise.vala:158
#, c-format
msgid "Xnoise is a media player for Gtk+."
msgstr ""

#: ../libxnoise/Application/xnoise-application.vala:245 ../src/xnoise.vala:159
msgid "It uses the gstreamer framework."
msgstr ""

#: ../libxnoise/Application/xnoise-application.vala:246 ../src/xnoise.vala:160
msgid "More information on the project website:"
msgstr ""

#. the 'no lyrics found...' also appears in the db provider !!
#: ../libxnoise/Database/xnoise-db-reader.vala:171
#: ../libxnoise/Lyrics/xnoise-lyrics-loader.vala:177
#: ../plugins/databaseLyrics/xnoise-database-lyrics.vala:127
msgid "no lyrics found..."
msgstr ""

#: ../libxnoise/Database/xnoise-db-writer.vala:601
#, c-format
msgid "unknown"
msgstr ""

#: ../libxnoise/DBus/xnoise-desktop-notifications.vala:156
#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:440
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1428
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1456
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1498
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1528
#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:231
#: ../plugins/titleToDecoration/xnoise-title-to-decoration.vala:134
#: ../plugins/titleToDecoration/xnoise-title-to-decoration.vala:157
#, c-format
msgid "by"
msgstr ""

#: ../libxnoise/DBus/xnoise-desktop-notifications.vala:159
#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:442
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1430
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1458
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1500
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1530
#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:233
#: ../plugins/titleToDecoration/xnoise-title-to-decoration.vala:136
#: ../plugins/titleToDecoration/xnoise-title-to-decoration.vala:159
#, fuzzy, c-format
msgid "on"
msgstr "തി"

#: ../libxnoise/DockableMedia/History/xnoise-dockable-playlist-lastplayed.vala:41
msgid "Last Played"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser-dockable.vala:47
msgid "Music"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser.vala:339
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-view.vala:522
#: ../plugins/magnatune/magnatune-treeview.vala:221
msgid "Collapse all"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser.vala:344
msgid "Sort Mode"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser.vala:354
msgid "ARTIST-ALBUM-TITLE"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser.vala:361
msgid "GENRE-ARTIST-ALBUM"
msgstr ""

#: ../libxnoise/DockableMedia/MusicBrowser/xnoise-music-browser-model.vala:534
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-store.vala:412
#: ../plugins/magnatune/magnatune-treestore.vala:43
msgid "Loading ..."
msgstr ""

#: ../libxnoise/DockableMedia/MostPlayed/xnoise-dockable-playlist-mostplayed.vala:41
msgid "Most Played"
msgstr ""

#: ../libxnoise/DockableMedia/StreamListing/xnoise-dockable-streams.vala:41
msgid "Streams"
msgstr ""

#: ../libxnoise/DockableMedia/VideoListing/xnoise-dockable-videos.vala:41
msgid "Videos"
msgstr ""

#: ../libxnoise/ExtDev/AndroidPlayer/xnoise-android-item-handler.vala:37
msgid "Add to Android Device"
msgstr ""

#: ../libxnoise/ExtDev/AndroidPlayer/xnoise-android-item-handler.vala:39
#: ../libxnoise/ExtDev/GenericPlayer/xnoise-generic-player-item-handler.vala:40
msgid "Delete from device"
msgstr ""

#: ../libxnoise/ExtDev/AndroidPlayer/xnoise-android-player-main-view.vala:45
msgid "Android Player Device"
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-item-handler.vala:103
msgid "Do you want to delete the selected file from the device?"
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-item-handler.vala:125
msgid "Do you want to delete the selected album from the device?"
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-item-handler.vala:155
msgid "Do you want to delete the selected artist from the device?"
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-item-handler.vala:235
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-view.vala:112
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-view.vala:341
msgid "Please wait while moving media to the device."
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-item-handler.vala:326
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-view.vala:260
#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-tree-view.vala:383
msgid "Not enough space on device! Aborting..."
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-main-view.vala:117
msgid "Free space: "
msgstr ""

#: ../libxnoise/ExtDev/AudioPlayer/xnoise-audio-player-main-view.vala:120
msgid "Total space: "
msgstr ""

#: ../libxnoise/ExtDev/GenericPlayer/xnoise-generic-player-device.vala:97
msgid "Player"
msgstr ""

#: ../libxnoise/ExtDev/GenericPlayer/xnoise-generic-player-item-handler.vala:38
msgid "Add to Player Device"
msgstr ""

#: ../libxnoise/ExtDev/GenericPlayer/xnoise-generic-player-main-view.vala:45
msgid "External Player Device"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:155
msgid "Add or Remove media"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:168
msgid "If selected, all media folders will be fully rescanned"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:174
msgid "Add local folder"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:175
msgid "Add media stream"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:176
msgid "Remove"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:179
msgid "Do full rescan"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:180
msgid ""
"Select local media folders or internet media streams. \n"
"All media sources will be available via xnoise's library."
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:218
#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:189
msgid "Location"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:258
#: ../libxnoise/Utils/xnoise-media-importer.vala:101
#: ../libxnoise/Utils/xnoise-media-importer.vala:235
msgid "Importing media data. This may take some time..."
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:274
#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:70
msgid "Select media folder"
msgstr ""

#: ../libxnoise/ExtraWidgets/AddMedia/xnoise-add-media-widget.vala:350
msgid "Add internet radio link"
msgstr ""

#: ../libxnoise/ExtraWidgets/Fullscreen/xnoise-fullscreen-toolbar.vala:259
msgid "Leave fullscreen"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:319
msgid "Settings"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:321
msgid "Media"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:327
msgid "Grid lines in media browser"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:328
msgid "Showing lines in the media browser might show hierachies more clear"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:332
msgid "Use systray icon"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:333
msgid ""
"Use a status icon on your panel for showing, hiding and controlling xnoise"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:337
msgid "Show picture on hover album image"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:338
msgid ""
"Show a larger view on the album art while the mouse pointer is over the "
"album image in the main window"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:342
msgid "Use menu button"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:343
msgid "Use an application menu button integrated into the main window"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:347
msgid "Show Stop button"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:348
msgid "Show a stop button along with the other playback control buttons"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:352
msgid "Quit on window close"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:353
msgid "Quit xnoise if the main window is closed"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:357
msgid "Use desktop notifications"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:358
msgid ""
"Use desktop notifications that inform about played media while the main "
"window of xnoise is not visible"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:362
msgid "Use combo box selector for media types"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:363
msgid "Use a combo box for selecting media types like music, video or streams"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:375
msgid "Go Back"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:384
msgid "Media browser fontsize"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:401
#, fuzzy
msgid "User Interface:"
msgstr "പേരിടാത്ത"

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:405
msgid "Lyrics:"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:409
msgid "Additional:"
msgstr ""

#: ../libxnoise/ExtraWidgets/Settings/xnoise-settings-widget.vala:413
msgid "Music Stores:"
msgstr ""

#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:65
msgid "Select external subtitle file"
msgstr ""

#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:145
msgid "No Subtitle"
msgstr ""

#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:203
msgid "Leave Fullscreen"
msgstr ""

#: ../libxnoise/ExtraWidgets/VideoScreen/xnoise-videoscreen.vala:203
msgid "Fullscreen"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-media-source-widget.vala:126
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2040
msgid "Search..."
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-equalizer-widget.vala:214
msgid "Preset:"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-equalizer-widget.vala:233
msgid "Volume"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:116
msgid "Add Media"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:118
msgid "Skip"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:123
msgid "Done"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:131
msgid "Please wait while media is added to your library!"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:132
#, c-format
msgid "You can start listening to your music by selecting '%s'"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:138
msgid "Add more media folders"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:161
msgid "Media Folders:"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:199
msgid "Welcome to Xnoise!\n"
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:201
msgid "This is the first time you start xnoise."
msgstr ""

#: ../libxnoise/ExtraWidgets/xnoise-first-start-widget.vala:203
msgid "Do you want to import media into your library?"
msgstr ""

#: ../libxnoise/ItemHandlers/AddAllToTracklist/xnoise-handler-add-all-to-tracklist.vala:41
msgid "Add all tracks to tracklist"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:62
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:66
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:128
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:73
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:77
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:167
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:143
msgid ""
"With this dialog you can change the metatags in the according files. \n"
"Handle with care!"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:136
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:185
msgid "xnoise - Edit metadata"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:139
msgid "Type new artist name."
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:140
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:181
msgid "Artist:"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:143
msgid "Type new album name."
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:144
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:182
msgid "Album:"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:163
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:203
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:200
msgid ""
"Please wait while filling media browser. Or cancel, if you do not want to "
"wait."
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-artist-editor.vala:167
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:207
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:204
msgid ""
"Please wait while importing media. Or cancel, if you do not want to wait."
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:177
msgid "Album data"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:180
msgid "Please enter new album data."
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:183
msgid "Year:"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-album-editor.vala:184
msgid "Genre:"
msgstr ""

#. ARTIST
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:146
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2050
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1237
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1367
msgid "Artist"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:149
msgid "Album Artist"
msgstr ""

#. ALBUM
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:152
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2051
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1213
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1376
msgid "Album"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:155
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1189
msgid "Title"
msgstr ""

#. GENRE
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:158
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2052
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1283
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1385
msgid "Genre"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:161
#, fuzzy
msgid "Uri"
msgstr "വെ"

#. YEAR
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:164
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2053
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1307
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1394
msgid "Year"
msgstr ""

#. TRACKNUMBER
#: ../libxnoise/ItemHandlers/EditTags/xnoise-tag-title-editor.vala:167
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1358
msgid "Tracknumber"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-handler-edit-tags.vala:35
#: ../libxnoise/ItemHandlers/EditTags/xnoise-handler-edit-tags.vala:39
msgid "Edit data for track"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-handler-edit-tags.vala:43
msgid "Change album data"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-handler-edit-tags.vala:47
msgid "Change artist data"
msgstr ""

#: ../libxnoise/ItemHandlers/EditTags/xnoise-handler-edit-tags.vala:51
msgid "Change genre name"
msgstr ""

#: ../libxnoise/ItemHandlers/AddToTracklist/xnoise-handler-add-to-tracklist.vala:50
#: ../libxnoise/ItemHandlers/AddToTracklist/xnoise-handler-add-to-tracklist.vala:55
msgid "Add to tracklist"
msgstr ""

#: ../libxnoise/ItemHandlers/MoveToTrash/xnoise-handler-move-to-trash.vala:41
msgid "Move to trash"
msgstr ""

#: ../libxnoise/ItemHandlers/MoveToTrash/xnoise-handler-move-to-trash.vala:87
msgid "Do you want to move the selected file to trash?"
msgstr ""

#: ../libxnoise/ItemHandlers/PlayItem/xnoise-handler-play-item.vala:35
msgid "Play"
msgstr ""

#: ../libxnoise/ItemHandlers/RemoveTrack/xnoise-handler-remove-track.vala:35
msgid "Remove selected track"
msgstr ""

#: ../libxnoise/ItemHandlers/ShowInFilemanager/xnoise-handler-show-in-file-manager.vala:35
#: ../libxnoise/ItemHandlers/ShowInFilemanager/xnoise-handler-show-in-file-manager.vala:39
msgid "Show in parent folder"
msgstr ""

#: ../libxnoise/Lyrics/xnoise-lyrics-view.vala:99
#: ../libxnoise/Lyrics/xnoise-lyrics-view.vala:154
msgid "Insufficient track information. Not searching for lyrics."
msgstr ""

#: ../libxnoise/Lyrics/xnoise-lyrics-view.vala:127
#: ../libxnoise/Lyrics/xnoise-lyrics-view.vala:143
msgid "Player stopped. Not searching for lyrics."
msgstr ""

#. Gtk.TextIter start_iter;
#. Gtk.TextIter end_iter;
#. textbuffer.get_start_iter (out start_iter);
#. textbuffer.get_start_iter (out end_iter);
#. string txt = textbuffer.get_text(start_iter, end_iter, true);
#. TODO: Howto append text ?
#: ../libxnoise/Lyrics/xnoise-lyrics-view.vala:179
#, c-format
msgid ""
"\n"
"Trying to find lyrics for \"%s\" by \"%s\"\n"
"\n"
"Using %s ..."
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:220
msgid "_File"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:221
msgid "open file"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:222
msgid "Open _Stream"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:222
msgid "open remote location"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:223
msgid "_Add or Remove media"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:223
msgid "manage the content of the xnoise media library"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:225
msgid "_Edit"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:226
msgid "C_lear tracklist"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:226
msgid "Clear the tracklist"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:227
msgid "_Rescan collection"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:227
msgid "Rescan collection"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:228
msgid "_Increase volume"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:228
msgid "Increase playback volume"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:229
msgid "_Decrease volume"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:229
msgid "Decrease playback volume"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:230
msgid "_Previous track"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:230
msgid "Go to previous track"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:231
msgid "_Toggle play"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:231
msgid "Toggle playback status"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:232
msgid "_Next track"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:232
msgid "Go to next track"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:234
#: ../plugins/ubuntuone/ubuntuone.vala:230
msgid "_View"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:235
msgid "_Tracklist"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:235
msgid "Go to the tracklist."
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:236
msgid "_Lyrics"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:236
msgid "Go to the lyrics view."
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:237
msgid "_Now Playing"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:238
msgid "Go to the 'Now Playing' screen in the main window."
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:239
msgid "_Help"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:241
msgid "_Frequently Asked Questions"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:241
msgid "_Open Frequently Asked Questions in web browser"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:242
msgid "_Keyboard Shortcuts"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:242
msgid "_Open Keyboard Shortcuts in web browser"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:243
msgid "_Config"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:302
msgid "_Show Media Browser"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:311
msgid "Show _Video Fullscreen"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:320
msgid "Show _Album Art view"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:512
#, c-format
msgid ""
"Restoring %u tracks in the tracklist. This is a large number and can make "
"startup of xnoise slower."
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:713
#: ../libxnoise/MainWindow/xnoise-main-window.vala:722
#: ../libxnoise/MainWindow/xnoise-main-window.vala:731
#: ../libxnoise/MainWindow/xnoise-main-window.vala:740
msgid "Playback mode: "
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:713
msgid "No repeat, one after another"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:722
msgid "Repeat single track"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:731
msgid "Repeat all"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:740
msgid "Random playlist track playing"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1329
msgid "Enter the URL for playing"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1345
msgid "Select media file"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1656
msgid "Remove selected titles"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1679
msgid "Remove all"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1703
msgid "Jump to current position"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1849
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2036
msgid "Select search with <Ctrl-F>"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1851
#: ../libxnoise/MainWindow/xnoise-main-window.vala:2038
msgid "Remove search filter with <Ctrl-D>"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1862
msgid "Open equalizer"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1887
msgid "Equalizer"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1914
msgid "Toggle visibility of album art view"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:1916
msgid "<Ctrl+B>"
msgstr ""

#. AppMenuButton for compact layout
#: ../libxnoise/MainWindow/xnoise-main-window.vala:1964
msgid "Show application main menu"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:2054
msgid "Playcount"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:2058
msgid "Ascending"
msgstr ""

#: ../libxnoise/MainWindow/xnoise-main-window.vala:2060
msgid "Descending"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:153
msgid "Dance"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:158
msgid "Pop"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:163
msgid "Techno"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:168
msgid "Club"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:173
msgid "Jazz"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:178
msgid "Rock"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:183
msgid "Maximum Treble"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:188
msgid "Maximum Bass"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:193
msgid "Classic"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:199
msgid "Custom"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-equalizer.vala:205
msgid "Default"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:229
msgid ""
"The subtitle name is not matching the video name! Not using subtitle file."
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:747
#, c-format
msgid "Success on installing missing gstreamer plugin"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:751
#, c-format
msgid "User aborted installation of missing gstreamer plugin"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:755
#, c-format
msgid "Gstreamer plugin not found in repositories"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:761
#, c-format
msgid "Critical error while installation of missing gstreamer plugin"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:806
#: ../libxnoise/Player/xnoise-gst-player.vala:817
#: ../libxnoise/Player/xnoise-gst-player.vala:830
#, c-format
msgid "Missing gstreamer plugin"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:808
#, fuzzy
msgid "Automatic missing codec installation not supported"
msgstr "റദ്ദാക്കുവാന്‍ സാധിക്കുന്ന ഇനീഷ്യലൈസേഷന്‍ പിന്തുണയ്ക്കുന്നില്ല"

#: ../libxnoise/Player/xnoise-gst-player.vala:819
msgid "Failed to start automatic gstreamer plugin installation."
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:832
msgid "Trying to install missing gstreamer plugin"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:932
msgid "Subtitle #"
msgstr ""

#: ../libxnoise/Player/xnoise-gst-player.vala:944
msgid "Audio Track #"
msgstr ""

#: ../libxnoise/PluginModule/xnoise-plugin-switch-widget.vala:98
msgid "(Plugin)"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:56
msgid "unknown artist"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:57
msgid "unknown title"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:58
msgid "unknown album"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:60
msgid "Now Playing"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:61
msgid "Tracklist"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:62
msgid "Lyrics"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:63
msgid "Hide Library"
msgstr ""

#: ../libxnoise/Resources/xnoise-resources.vala:64
msgid "Show Library"
msgstr ""

#. LENGTH
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1259
#: ../libxnoise/TrackList/xnoise-tracklist.vala:1403
msgid "Length"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:87
msgid "Previous"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:102
msgid "Next"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:120
msgid "Exit"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:169
#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:181
msgid "stopped"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:173
msgid "playing"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:177
msgid "paused"
msgstr ""

#: ../libxnoise/TrayIcon/xnoise-tray-icon.vala:189
#, c-format
msgid "ready to rock"
msgstr ""

#: ../libxnoise/Utils/xnoise-media-importer.vala:355
#, c-format
msgid "Found %u tracks. Updating library ..."
msgstr ""

#: ../libxnoise/Utils/xnoise-media-importer.vala:570
#, c-format
msgid "%u new tracks found"
msgstr ""

#: ../src/xnoise.vala:169
msgid ""
"Run 'xnoise --help' to see a full list of available command line options.\n"
msgstr ""

#: ../plugins/databaseLyrics/DatabaseLyrics.xnplugin.desktop.in.h:1
msgid "Database buffer for lyrics"
msgstr ""

#: ../plugins/databaseLyrics/DatabaseLyrics.xnplugin.desktop.in.h:2
msgid "Buffer lyrics in database"
msgstr ""

#: ../plugins/lastfm/xnoise-lastfm.vala:401
#: ../plugins/lastfm/xnoise-lastfm.vala:523
#, c-format
msgid "User logged in!"
msgstr ""

#. feedback
#: ../plugins/lastfm/xnoise-lastfm.vala:405
#: ../plugins/lastfm/xnoise-lastfm.vala:521
#: ../plugins/lastfm/xnoise-lastfm.vala:526
#, c-format
msgid "User not logged in!"
msgstr ""

#: ../plugins/lastfm/xnoise-lastfm.vala:466
msgid "Visit LastFm for an account."
msgstr ""

#: ../plugins/lastfm/xnoise-lastfm.vala:475
#, c-format
msgid "Please enter your lastfm username and password."
msgstr ""

#: ../plugins/lastfm/xnoise-lastfm.vala:484
#: ../plugins/magnatune/magnatune.vala:257
#, fuzzy, c-format
msgid "Username:"
msgstr "പേരിടാത്ത"

#: ../plugins/lastfm/xnoise-lastfm.vala:493
#: ../plugins/magnatune/magnatune.vala:266
#, c-format
msgid "Password:"
msgstr ""

#: ../plugins/lastfm/xnoise-lastfm.vala:510
msgid "Scrobble played tracks on lastfm (Send song data to create statistis)"
msgstr ""

#: ../plugins/testplugin/xnoisetest.xnplugin.desktop.in.h:1
msgid "Basic Demo Plugin for Developers"
msgstr ""

#: ../plugins/testplugin/xnoisetest.xnplugin.desktop.in.h:2
msgid "Demo plugin for xnoise"
msgstr ""

#: ../plugins/chartlyrics/chartlyrics.xnplugin.desktop.in.h:1
msgid "Chartlyrics"
msgstr ""

#: ../plugins/chartlyrics/chartlyrics.xnplugin.desktop.in.h:2
msgid "Load lyrics from chartlyrics.com"
msgstr ""

#: ../plugins/lastfm/lastfm.xnplugin.desktop.in.h:1
msgid "LastFm web service integration"
msgstr ""

#: ../plugins/lastfm/lastfm.xnplugin.desktop.in.h:2
msgid "Use lastfm's web services"
msgstr ""

#: ../plugins/lyricwiki/Lyricwiki.xnplugin.desktop.in.h:1
msgid "Lyricwiki"
msgstr ""

#: ../plugins/lyricwiki/Lyricwiki.xnplugin.desktop.in.h:2
msgid "Load lyrics from http://www.lyricwiki.com"
msgstr ""

#: ../plugins/azlyrics/azlyrics.xnplugin.desktop.in.h:1
msgid "Azlyrics"
msgstr ""

#: ../plugins/azlyrics/azlyrics.xnplugin.desktop.in.h:2
msgid "Load lyrics from azlyrics.com"
msgstr ""

#: ../plugins/mediakeys/mediakeys.xnplugin.desktop.in.h:1
msgid "Media Control Keys"
msgstr ""

#: ../plugins/mediakeys/mediakeys.xnplugin.desktop.in.h:2
msgid "Control xnoise using mediakeys"
msgstr ""

#: ../plugins/magnatune/Magnatune.xnplugin.desktop.in.h:1
msgid "Magnatune Music Subscription"
msgstr ""

#: ../plugins/magnatune/Magnatune.xnplugin.desktop.in.h:2
msgid "Magnatune Music Store"
msgstr ""

#: ../plugins/magnatune/magnatune-dockable.vala:57
#: ../plugins/magnatune/magnatune.vala:216
msgid "Magnatune"
msgstr ""

#: ../plugins/magnatune/magnatune-treeview.vala:230
msgid "Download whole album to disk"
msgstr ""

#: ../plugins/magnatune/magnatune-treeview.vala:280
msgid "Downloading album "
msgstr ""

#: ../plugins/magnatune/magnatune-treeview.vala:282
msgid "This may take some time..."
msgstr ""

#: ../plugins/magnatune/magnatune-treeview.vala:396
msgid "Download finished for \""
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:234
msgid "download finished..."
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:238
msgid "decompressing..."
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:292
msgid "decompressing finished..."
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:313
msgid ""
"Please wait while\n"
"converting database."
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:360
#, c-format
msgid ""
"Please wait while\n"
"converting database.\n"
"Done for %d tracks."
msgstr ""

#: ../plugins/magnatune/magnatune-widget.vala:380
msgid "loading..."
msgstr ""

#: ../plugins/magnatune/magnatune.vala:173
msgid "Username and Password available"
msgstr ""

#: ../plugins/magnatune/magnatune.vala:174
msgid "Username or Password not available"
msgstr ""

#: ../plugins/magnatune/magnatune.vala:236
msgid "Visit Magnatune for an account."
msgstr ""

#: ../plugins/magnatune/magnatune.vala:247
#, c-format
msgid "Please enter your Magnatune username and password."
msgstr ""

#: ../plugins/mpris/mpris.xnplugin.desktop.in.h:1
msgid "Mpris v2 Dbus Service"
msgstr ""

#: ../plugins/mpris/mpris.xnplugin.desktop.in.h:2
msgid "Control xnoise with a MPRIS v2 dbus interface"
msgstr ""

#: ../plugins/mpris_one/mpris_one.xnplugin.desktop.in.h:1
msgid "Mpris v1 Dbus Service"
msgstr ""

#: ../plugins/mpris_one/mpris_one.xnplugin.desktop.in.h:2
msgid "Control xnoise with a MPRIS v1 dbus interface"
msgstr ""

#: ../plugins/soundmenu2/soundmenu2.xnplugin.desktop.in.h:1
msgid "Ubuntu Soundmenu Integration"
msgstr ""

#: ../plugins/soundmenu2/soundmenu2.xnplugin.desktop.in.h:2
msgid "Use ubuntu/ayatana soundmenu integration"
msgstr ""

#: ../plugins/titleToDecoration/TitleToDecoration.xnplugin.desktop.in.h:1
msgid "Show song title in window bar"
msgstr ""

#: ../plugins/titleToDecoration/TitleToDecoration.xnplugin.desktop.in.h:2
msgid "Set the window name to the current title"
msgstr ""

#: ../plugins/ubuntuone/UbuntuOne.xnplugin.desktop.in.h:1
msgid "UbuntuOne Music Store"
msgstr ""

#: ../plugins/ubuntuone/UbuntuOne.xnplugin.desktop.in.h:2
msgid "Ubuntu One Music Store"
msgstr ""

#: ../plugins/ubuntuone/ubuntuone.vala:128
msgid "UbuntuOne Music"
msgstr ""

#: ../plugins/ubuntuone/ubuntuone.vala:163
msgid "UbuntuOne"
msgstr ""

#: ../plugins/ubuntuone/ubuntuone.vala:231
msgid "Show _UbuntuOne Store"
msgstr ""

#: ../plugins/ubuntuone/ubuntuone.vala:231
msgid "Show UbuntuOne Store"
msgstr ""

#, fuzzy
#~ msgid "no repeat"
#~ msgstr "ആവര്‍ത്തനത്തിന്റെ ആവശ്യകതയില്ല"
